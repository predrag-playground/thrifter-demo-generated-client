defmodule ThrifterDemo do
  use Application

  alias ThrifterDemo.Config

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    host                = Config.get!(:host)
    port                = Config.get_number!(:port)
    connection_timeout  = Config.get_number(:timeout) || 3000

    children = [
      worker(ThrifterDemo.Client, [{host, port}, connection_timeout]),
    ]

    opts = [strategy: :one_for_one, name: ThrifterDemo.Supervisor]
    Supervisor.start_link(children, opts)
  end

end

defmodule ThrifterDemo.Config do
  def get(name) do
    case Application.get_env(:thrifter_demo, name) do
      {:system, variable_name} -> System.get_env(variable_name)
      value                    -> value
    end
  end

  def get_number(name) do
    case get(name) do
      nil   -> nil
      value -> Integer.parse(value) |> elem(0)
    end
  end

  def get!(name) do
    get(name) || raise "Parameter #{name} for thrifter_demo is not set"
  end

  def get_number!(name) do
    get_number(name) || raise "Parameter #{name} for thrifter_demo is not set"
  end
end

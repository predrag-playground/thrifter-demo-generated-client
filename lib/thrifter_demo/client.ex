defmodule ThrifterDemo.Client do
  use Riffed.Client,
    auto_import_structs: true,
    service: :test_thrift,
    structs: ThrifterDemo.Models,
    client_opts: [
      retries: 3,
      framed: true],
    import: [:add, :sub]

  def start_link({host, port}, connection_timeout) do
    options = [name: __MODULE__, timeout: connection_timeout]
    GenServer.start_link(__MODULE__, {host, port}, options)
  end
end
